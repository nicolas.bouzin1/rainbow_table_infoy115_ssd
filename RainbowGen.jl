using DelimitedFiles
using Random
using SHA
using BenchmarkTools
using Dates

function generate_hashes(amount, length_pwd)
    """Generate a list of passwords hashed in SHA256

    Args:
        amount: How many passwords
        pwd_length: The length of the passwords use to generate the table (between 6 and 10 included following the project's guidelines).
    Output: File with password:password_hash
    """
    passwords = [];
    for i in 1:amount
        password = randstring(['A':'Z'; 'a':'z';'0':'9'], length_pwd);
        while password in passwords
            password = password = randstring(['A':'Z'; 'a':'z';'0':'9'], length_pwd);
        end
        push!(passwords,(password, bytes2hex(sha256(password))));
    end
    writedlm("passwords.txt",passwords);
end

function generate_chain(password, chain_length = 50000)
    """Generate a chain for a given password

        Args:
            password: Starting password
            chain_length(optional): Define the length of the chain
    Output: last hashed password of the chain
    """
    reduced = password
    hashed="";
    for i in 1:chain_length
      hashed = bytes2hex(sha256(reduced));
      reduced = reduce_hash(hashed,i,i % 10, length(password));
    end
    return hashed
end


const characters = ['A':'Z'; 'a':'z';'0':'9'];
const n_chars = length(characters);


struct RainbowTable
    table::Dict{String,String}
    function RainbowTable()
        table = Dict{String,String}()
        return new(table)
    end
end


"""
    generate_table!

Generate a rainbow table

Args:
    RT: A rainbow table mutable struct with one table field.
    size: The amount of head/tail pairs we want in our rainbow table.
    pwd_length: The length of the passwords use to generate the table (between 6 and 10 included following the project's guidelines).
    chain_length(int, optional): Length of each chain (=amount of columns). Defaults to 50000.
"""
function generate_table!(RT, size, pwd_length, chain_length = 50000)
    # buffers used for reduction chains
    buffers = [IOBuffer() for _ in 1:Threads.nthreads()]
    # generate unique psw's beforehand (this is fast, we can do without parallelism, by using a set we avoid double values)
    # collusion counting was removed (should be added if needed)
    passwords = Set{String}()
    while length(passwords) < size
        push!(passwords, randstring(characters, pwd_length))
    end
    passwords =  collect(passwords) # convert to vector for indexing
    # generate chains
    chains = Vector{String}(undef, size)
    # then parse them using multithreading
    Threads.@threads for i = 1:size 
        @inbounds chains[i] = generate_chain(passwords[i], chain_length; buffer=buffers[Threads.threadid()])
    end

    for i = 1:size
        RT.table[chains[i]] = passwords[i]
    end   
    #=
    another option would be return Dict(zip(chains, passwords)).
    =#

    return
end

function reduction(hash, index, extra, i)
    return characters[(((Int(only(hash[i + extra])) ⊻ index) + Int(only(hash[i + 12]))) % (n_chars))+1]
end


"""
    reduce_hash

Reduction function

Args:
    hash: Hash to reduce
    index: Place of reduction function in the chain (=column)
    extra: extra parameter to have kind of multiple reduction functions to avoid collisions
Output: Valid password
"""
function reduce_hash(hash, index, extra, pwd_length; buffer::IOBuffer=IOBuffer())
    pswLength = pwd_length;
    for i in 1:pswLength
        write(buffer, reduction(hash, index, extra, i)) 
    end
    return String(take!(buffer))
end

"""Generate a chain for a given password

Args:
    password: Starting password
    chain_length(optional): Define the length of the chain
Output: last hashed password of the chain
"""
function generate_chain(password, chain_length = 50000; buffer::IOBuffer=IOBuffer())
    reduced = password
    hashed="";
    for i in 1:chain_length
      hashed = bytes2hex(sha256(reduced));
      reduced = reduce_hash(hashed,i,i % 10, length(password), buffer=buffer);
    end
    return hashed
end


function main()
    if length(ARGS) == 0
        println("Please give an input. Use -h for help")
        return
    elseif ARGS[1] == "-h"
        print("Usage : RainbowGen.py -options arguments \n")
        print("-h print help \n")
        print("-g x y : generate x passwords/hashes combinations. y is the length of the passwords. \n")
        print("-t x y : generate a rainbowtable of size. y is the length of the passwords. x \n")
    elseif ARGS[1]=="-g"
        generate_hashes(parse(Int64,ARGS[2]),parse(Int64,ARGS[3]));
    elseif ARGS[1]=="-t"
        tstart = now()
        T = RainbowTable();
        generate_table!(T, parse(Int64,ARGS[2]), parse(Int64,ARGS[3]))
        writedlm("RT.txt", T.table)
        tend = now()
        dt = tend-tstart
        println("Generated a table of length $(parse(Int64,ARGS[2])) in $(dt)")
        return dt.value / 1000 # return time in seconds
    end
end

main()

