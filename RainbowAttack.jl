using DelimitedFiles
using Random
using SHA
using Dates


const characters = ['A':'Z'; 'a':'z';'0':'9'];
const n_chars = length(characters);


function reduction(hash, index, extra, i)
    return characters[(((findfirst(==(hash[i + extra]), characters) ⊻ index) + Int(only(hash[i + 12]))) % (n_chars))+1]
end


"""
    reduce_hash

Reduction function

Args:
    hash: Hash to reduce
    index: Place of reduction function in the chain (=column)
    extra: extra parameter to have kind of multiple reduction functions to avoid collisions
Output: Valid password
"""
function reduce_hash(hash, index, extra, pwd_length; buffer::IOBuffer=IOBuffer())
    pswLength = pwd_length;
    for i in 1:pswLength
        write(buffer, reduction(hash, index, extra, i)) 
    end
    return String(take!(buffer))
end

function hash_pwd(pwd)
    return bytes2hex(sha256(pwd))
end

struct RainbowTable
    table::Dict{String,String}
    function RainbowTable(file)
        """
        import the data from the previously generated rainbowtable
        """
        table = Dict{String,String}()
        for line in readlines(file)
            line2 = split(line,"\t")
            table[line2[1]] = line2[2];
        end
        return new(table)
    end
end

function traverse_chain(chain_length, hash, start)
    """
    Traverses a chain in our table to find the plaintext password. Assumes we already have confirmation that hash is crackable with current RT. If not the case, returns None.

        Args:
            chain_length : the chain length used to generate the RT we are using
            hashedPass : The hash to crack
            start : Start of chain
    """
    reduced = start;
    for i = 1:chain_length
        test_hash = hash_pwd(reduced);
        if test_hash == hash
            return reduced
        else
            reduced = reduce_hash(test_hash, i, i % 10, length(start))
        end
    end
end

function crack(chain_length, RT, hash_to_crack)
    traversalResult = "false"
    val = collect(values(RT.table));
    pwd_length = length(val[1]);
    start = now()
    if hash_to_crack in keys(RT.table)
        cracked = traverse_chain(chain_length, hash_to_crack, RT.table[hash_to_crack])
        #@info "\n $(hash_to_crack) is the hash of $(cracked). The hash was cracked in $(now()-start) seconds. "
        return cracked
    end
    for i = chain_length-1:-1:1 #From last reduction function
        candidate = hash_to_crack
        for index = i:chain_length-1
            candidate = hash_pwd(reduce_hash(candidate,index, index % 10, pwd_length))
        end
        if candidate in keys(RT.table)
            #@info "$(hash_to_crack) is possible to be cracked with current RT."
            traversalResult = traverse_chain(chain_length, hash_to_crack,RT.table[candidate])
            #@info "$(hash_to_crack) is the hash of $(traversalResult). The hash was cracked in $(now()-start) seconds. "
            break
        end
    end
    if traversalResult=="false" || traversalResult===nothing
        #@warn "The hash $(hash_to_crack) could not be cracked with current RT."
    end
    return traversalResult
end

function crack_hashes(chain_length, RT, hash_file)
    """Cracks hashes from a file,

    Args:
        hash_file : .txt file with one hash per line to crack
        result_file : .txt file where we need to write our results to
    """
    partial_tables = [Dict{String,String}() for _ in 1:Threads.nthreads()]
    Threads.@threads for hashed in readlines(hash_file)
        passw = crack(chain_length, RT, hashed)
        if typeof(passw)==String && passw != "false"
            partial_tables[Threads.threadid()][hashed] = passw
        end
    end
    total_table = Dict{String,String}()
    for ii = 1:length(partial_tables)
        total_table = merge(total_table, partial_tables[ii])
    end
    return total_table
end

function main()
    if length(ARGS) == 0
        println("Please give an input. Use -h for help")
        return
    elseif ARGS[1] == "-h"
        println("Usage : RainbowAttack.py -options arguments")
        println("-h print help")
        println("-c RT x y :  Attempt to crack the hashes in the x file via the rainbow table from the RT path and saving the results in the y file")
    elseif ARGS[1] == "-c"
        tstart = now()
        T = RainbowTable(ARGS[2])
        cracks = crack_hashes(parse(Int64,ARGS[5]), T, ARGS[3])
        writedlm(ARGS[4],cracks)
        dt = now()-tstart
        @info "$(length(cracks)) out of $(length(readlines(ARGS[3]))) hashes are cracked, success rate: $(round(length(cracks)*100/length(readlines(ARGS[3])); digits = 2))%. This has been achieved in $(dt) miliseconds."
    end
end

main()






