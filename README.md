# Rainbow attack 

Rainbow table attack implementation for our project related to InfoY115-SSD 

## Group members
- BOUZIN Nicolas: 000474869
- COLLIGNON Jiangdong: 000474853
- PHILIPPOT Maxim: 000578521
- PRAKOPETZ Nathan: 000578324
- VAN DER VORST Quentin: 000579210

## Installation
Open a terminal and type the following commands:
```sh
# Make the file executable
chmod +x shell
# Run the file
./shell

```
**Remark:** your password will be required in order to create a symbolic link _julia_.

## Usage

- **Help**

```sh
julia ./RainbowGen.jl -h
julia ./RainbowAttack.jl -h
```
- **Generate password/hash combination**

With:
- x the amount of passwords we want to generate.
- y the length of the passwords we want to generate ($\in$ [6;10]).
```sh
julia ./RainbowGen.jl -g x y
```
- **Generate a rainbow table**

With:
- n the amount of threads we want to use.
- x the size of the rainbow table.
- y the length of the passwords considered in the rainbow table ($\in$ [6;10]).
```sh
julia -t n ./RainbowGen.jl -t x y
```

- **Perfrom a rainbow attack on a set of hashes**

With:
- n the amount of threads we want to use.
- the rainbow table file contains a rainbow table (**format**: _tail(hash)_ tab _head_).
- the hashes file contains the hashes (only the hashes) we want to crack.
- the results file is an empty .txt file where we are going to write the hashes and the corresponding cracked password.
- x the chain length used to generate the rainbow table we are using.

**Remark:** be sure to provide a rainbow table created for the good password length.

```sh
julia -t n ./RainbowAttack.jl -c "path_to_rainbow_table" "path_to_hashes_file" "path_to_results_file" x
```


## Limitations

- Even after a lot of optimizations, julia is still quite slow. It is already a lot better than python (initially chosen) but we definitely have to learn C++ for the next projects.
- Even if our reduction function seems good we have some collisions (not really a lot but enough to consider it). This results in a poor efficiency of the attack script.
